variable "ssh_port" {
  description = "Port pour les requêtes SSH"
  default = 22
}
variable "http_port" {
  description = "Port pour les requêtes HTTP"
  default = 80
}
resource "aws_security_group" "sg_ssh" {
  name = "sg_ssh"
  description = "Permettre le SSH depuis mon IP"

  ingress {
    from_port = var.ssh_port
    to_port   = var.ssh_port
    protocol  = "tcp"
    cidr_blocks = ["81.66.212.178/32"]
  }
  ingress {
    from_port = var.http_port
    to_port   = var.http_port
    protocol  = "tcp"
    cidr_blocks = ["81.66.212.178/32"]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

