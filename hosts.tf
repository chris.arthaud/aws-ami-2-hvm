resource "aws_instance" "Terraform-1" {
# Amazon Linux 2 AMI (HVM), SSD Volume Type
  ami			= "ami-040ba9174949f6de4"
  instance_type = "t2.micro"
  key_name = "webarthaud"
  tags = {
    Name = "Terraform-1"
  }
  security_groups = ["${aws_security_group.sg_ssh.name}"]
  user_data = file("postinstall.yml")
  
  provisioner "local-exec" {
    command = "echo ${aws_instance.Terraform-1.public_ip} > aws_example_ip.txt"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.Terraform-1.public_dns} > aws_example_dns.txt"
  }
}

output "public_ip" {
  value = aws_instance.Terraform-1.public_ip
}
output "public_dns" {
  value = aws_instance.Terraform-1.public_dns
}
